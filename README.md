qt_capture
===============

qt\_capture is an OpenGL capture program designed to work with [Open Broadcaster Software](https://obsproject.com/) (OBS) in a manner which allows 
for other programs to utilize OBS' features without the the client provided by obs-studio. 

Many GUI progams exist with their own layout, frameworks, 
and settings which can benefit largely from OBS' external plugins and libraries. 

Open Broadcaster Software, which is a video capture program with a plug-and-play oriented SDK, is released under GNU General Public License, version 2. 
As such, qt_capture is a derivative of OBS and is therefore released under the GPL v2 as well. 

OBS ships as a GUI program with a frontend, however its main functionality is performed via an API known as "libobs", which connects to a number of various other static and dynamic libraries. These libraries are used for processes such as hooking the target process to be recorded; interprocess communication via pipes and OS-specific event systems; threading; 
encoding APIs, and a number of plugins which are handled by the core libobs library. OBS indeed uses a plugin architecture; for example, "Game Capture" is the plugin which is used to capture the viewport of a graphics program based off of OpenGL or Direct3D. It is currently the only plugin supported by qt_capture at this time.

While obs-studio is cross platform (Windows, OSX, Linux), the client for this project, which uses the OBS libraries, 
has been designed to work only with Windows. There aren't any plans at this time to support other platforms.

Nearly all of its code base is written in C99, so it's necessary to have MSVC 2013 in order
to support these C99 features. There is a community edition available, for free, which works well.

The modified fork of OBS-Studio, which is needed for this client to work as intended, can be found [here](https://bitbucket.org/cadalog/cadalog-obs-studio).

##Building


###Requirements

* (All dependencies for libobs as well as qt_capture need to be compiled under MSVC 2013)

* Cmake

* Qt 5. 
	- Currently, the only qt libraries used are the core, gui, widgets, and networking modules - along with any of their required dependencies.
	- Make sure the environment variable $(QTDIR) is set to the directory in which Qt's bin, include, and lib files are located. 
		  This should be the case  by default, however sometimes this isn't.
* A means of moc'ing header files defined in qt_capture. The Qt VS plugin works well for this.

* FFmpeg

* libx264

###Steps

1) Build OBS. Consult the INSTALL file (found in the OBS repository) for instructions on how to do this. 

2) Build qt_capture:
 
You'll need to define some more environment variables, which are
    
* OBSSrcPath; the path to the root of the OBS source repository.

* OBSLibPath; the path to the root of the directory where CMake has built the OBS binaries.

* VCREDISTDIR; The location of the DLL redistribution directory for MSVC 2013. For example: "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\redist". Used for deploying release builds.

* Load up qt\_capture.sln and compile debug/release builds as necessary. Note that there is also a RelWithDebInfo, which uses less optimization settings and (obviously) compiles with debug information.

##API


The API is relatively simple. The client receives a JSON string with parameters through the standard arguments at the program's entry point.

It's important to be aware of the fact that the client currently *expects* the data to be the second argument. The first argument supplied is 
technically irrelevant, though it's usually the name of the program ("path/to/qt_capture.exe", in this case)

Currently, the following options are supported:
	
###Resolution and Frame settings

* base\_screen\_width (int) : the width of the target viewport.
* base\_screen\_height (int) : the height of the target viewport.
	
* scale\_screen\_width (int) : resolution of the output width; optional
* scale\_screen\_height (int) : resolution of the output height; optional
	- Note: if either scale\_screen\_width or scale\_screen\_height are 0, then no scaling will be applied.

* fps (int) : desired framerate

###Target Params

* window_title (string) : actual title of the target window to be captured.
* window_class (string) : the win32 API window class of the target window to be captured. 
* exe_name (string) : the name of the exe to capture, with the exe extension included.
	
	Technically, the above three are all optional, but you need at least the exe\_name or an accurate window\_title to ensure that the process works appropriately.

* save\_path (string) : the path of the recorded file 
	- Extension is derived from the save_path. Only  .mp4, and .flv can be guaranteed to work at this time. 
	- Technically, .avi is supported as well, however its rate of frame output vs the rate of the feed being captured is off
		  to the point where the two channels are out of sync. In other words, finishing a recording may result in an .avi
		  file with only half of the intended feed actually output. 

###Other 

* close\_delay (float) : an optional delay to be applied on shutdown, in seconds. In some cases, this can prevent a premature shutdown which would result in a file that only contains a
	                        certain portion of the captured frames written to it.

##Caveats

### Handling commas

t's important to be aware that any commas used in the string values which are found in the JSON need to be replaced with a pipe ('|') in order to be properly processed. Failure to do this may cause undefined behvaior.
For example, say we have the following json defined:

```
json = {
   some_path: "my/path (company, something)/to/somewhere",
   other_value: 3
};
```

Before sending this off to qt_capture's commandline params as a serialized string, we ned to make the following change:

```
json = {
   some_path: "my/path (company| something)/to/somewhere",
   other_value: 3
};
```

### Required format

If the serialized json is formatted like so:

```
"json = {
   \"some_path\" : \"my/path (company| something)/to/somewhere\",
   \"other_value\" : 3
}"
```

then the following changes must be made:

```
"json = {
   some_path:my/path (company| something)/to/somewhere,
   other_value:3
}"
```

Notice that all the escaped quotes are erased, in addition to any whitespace found on either side of the colon separator. This is for the sake of simplifying the parsing. 
It's likely that more modifications will be made to remove the need for this particular requirement in the future.