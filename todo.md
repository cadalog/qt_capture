# Should continue writing data until finished.
# Idea 
 	* in obs-ffmpeg-output, add uint32_t "frames_rendered" or something like that.
 	* add @frame_count for the animation player to the commandline arguments for qt_capture
 	* each time a frame in process_packet() (called in write_thread(), in the ffmpeg output file) has been written, increment frames_rendered
 	* add a "target_frame_count" option to the configuration; if this is non-zero,
 	  then any outside stop calls made to the write_thread should be performed if and only if the "frames_rendered"
 	  variable matches the target_frame_count integer from the configuration.
 	  Otherwise, a wait operation can be performed.
 	* In addition, add a mutex for frames_rendered, to prevent any odd reads from happening. 



