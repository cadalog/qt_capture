#include "util.h"
#include <sstream>

bool util_load_deps( void )
{
	WIN32_FIND_DATAA findFileData;

	// DLL load directory is independent to working directory, which
	// FindFirstFileA is dependent on.
	const char* loadExpr = "*"SHARED_EXT;

	HANDLE file = FindFirstFileA( loadExpr, &findFileData ); 

	assert( file != INVALID_HANDLE_VALUE );

	do 
	{	
		if ( !( !!LoadLibraryA( findFileData.cFileName ) ) )
		{
			std::stringstream err;
			err << "Failed to load \"" << findFileData.cFileName << "\". Need to exit."; 
			util_alert( err.str() );
			return false;
		}
	} 
	while ( FindNextFileA( file, &findFileData ) );

	return true;
}