#include "window.h"
#include "util.h"
#include <QApplication>
#include <stdio.h>
#include <assert.h>
#include <sstream>
#include <string>
#include <memory>
#include <Windows.h>
#include <tlhelp32.h>
#include <tchar.h>
#include <plugins/win-capture/graphics-hook-info.h>
#include <libobs/obs-source.h>


static MainWindow* window = nullptr;

// Pass "noarg" when running from the commandline to 
// omit the need for the JSON-esque data. This is useful
// for debugging simple things like dependency issues.
// Clearly, the result is primitive, but only because there is no need for 
// anything more complex at this time.
#define BASE_ARGUMENT_BYPASS "noarg"
#define BASE_ARGUMENT_INDEX 1

static char get_dir_sep( const std::string& s )
{
	// We may need to append a path separator, so we check here
	char slash = 0;
	const char* c = NULL;
	const char* src = s.c_str();

	c = strchr( src, '\\' );

	if ( c )
	{
		slash = *c;
	}
	else
	{
		c = strchr( src, '/' );
		if ( c )
			slash = *c;
	}

	return slash;
}

static void parse_args_json_bypass( int argc, char* argv[], int* wait, std::string& workingDir, capture_args& args )
{
#ifdef NDEBUG
	if ( wait )
	{
		*wait = 10; 
	}
#else
	UNUSED_PARAMETER( wait );
#endif

	args.fps = 60;
	args.baseWidth = 800;
	args.baseHeight = 600;
	
	workingDir.append( argv[ 2 ] );
	char slash = get_dir_sep( workingDir );

	if ( !slash )
		slash = '\\';

	args.saveLocation = workingDir;
	args.saveLocation.append( 1, slash );
	args.saveLocation.append( "test.mp4" );
}

// Arguments are supplied in a modified version of JSON, where escaped quotes denoting strings are removed. This is to avoid the shell
// misinterpreting the quotation values as separate arguments. White space which is initially wrapped around the 
// colon is also trimmed, for simpler parsing.
static void parse_args_json( const char* json, int* wait, std::string& workingDir, capture_args& args )
{
	char key[ 32 ];

	const char* p = json;

	size_t arglen = strlen( json ); 

	while ( ( uintptr_t )( p - json ) < arglen ) 
	{
		if ( *p == '{' )
		{
			p++;
		}

		const char* cutoff = strchr( p, ':' );
		const char* postCutoff = cutoff + 1;

		if ( cutoff )
		{
			memset( key, 0, sizeof( key ) );
			memcpy( key, p, sizeof( char ) * ( cutoff - p ) );

			char val[ 512 ];
			memset( val, 0, sizeof( val ) );
			const char* end = strchr( postCutoff, ',' );

			if ( !end )
			{
				end = strchr( postCutoff, '}' );
			}
			
			memcpy( val, postCutoff, sizeof( char ) * ( end - postCutoff ) );

			p = end + 1;

			if ( isspace( *p ) )
				p++;

			if ( strcmp( key, "base_screen_width" ) == 0 )
			{
				args.baseWidth = ( int ) strtol( val, ( char** ) nullptr, 10 ); 
			}
			else if ( strcmp( key, "base_screen_height" ) == 0 )
			{
				args.baseHeight = ( int ) strtol( val, ( char** ) nullptr, 10 );
			}
			else if ( strcmp( key, "scale_screen_width" ) == 0 )
			{
				args.scaleWidth = ( int ) strtol( val, ( char** ) nullptr, 10 );
			}
			else if ( strcmp( key, "scale_screen_height" ) == 0 )
			{
				args.scaleHeight = ( int ) strtol( val, ( char** ) nullptr, 10 );
			}
			else if ( strcmp( key, "fps" ) == 0 )
			{
				args.fps = ( int ) strtol( val, ( char** ) nullptr, 10 );
			}
			else if ( strcmp( key, "close_delay" ) == 0 )
			{
				args.closeDelay = strtof( val, ( char** )nullptr );
			}
			else if ( wait && strcmp( key, "wait" ) == 0 )
			{
				*wait = ( int ) strtol( val, ( char** ) nullptr, 10 );
			}
			else if ( strcmp( key, "working_dir" ) == 0 )
			{
				workingDir.append( val );
			}
			else if ( strcmp( key, "save_path" ) == 0 )
			{
				args.saveLocation.append( val );
			}
			else if ( strcmp( key, "exe_name" ) == 0 )
			{
				args.exeName.append( val );
			}
			else if ( strcmp( key, "window_class" ) == 0 )
			{
				args.windowClass.append( val );
			}
			else if ( strcmp( key, "window_title" ) == 0 )
			{
				args.windowTitle.append( val );
			}
		}
		else
		{
			break;
		}
	}
}

static void sanitize( std::string& s )
{
	int i = 0;
	while ( ( i = s.find( '|', i + 1 ) ) != std::string::npos )
	{
		s.replace( i, 1, 1, ',' );
	}
}

int main( int argc, char* argv[] ) 
{	
#ifndef NDEBUG
	DebugActiveProcess( GetCurrentProcessId() );
#endif

	// Parse the arguments
	capture_args args;
	std::string workingDir;

	int wait = -1;
	if ( strcmp( argv[ BASE_ARGUMENT_INDEX ], BASE_ARGUMENT_BYPASS ) == 0 )
	{
		parse_args_json_bypass( argc, argv, &wait, workingDir, args );
	}
	else
	{
		parse_args_json( argv[ BASE_ARGUMENT_INDEX ], &wait, workingDir, args );
	}

	// For debugging purposes
	if ( wait != -1 )
	{
		Sleep( wait * 1000 );
	}

	sanitize( workingDir );
	sanitize( args.saveLocation );

	DirStack dirSave;
	
	// Save the string so we can use it to reset its value later
	std::string prevDir( workingDir.c_str(), workingDir.size() );

	dirSave.SetWorkingDir( prevDir );
	
	char slash = get_dir_sep( workingDir );

	if ( !slash )
	{
		util_alert( "Cannot find a directory separator! The save filepath you requested is invalid. Exiting." );
		return 1;
	}

	// Need a trailing slash?
	if ( workingDir.back() != slash )
		workingDir.append( 1, slash );

	// Add the directory where the main libOBS deps are located;
	// all modules (i.e., plugins) used by libOBS will be loaded relative
	// to the bin directory during libOBS' initialization
	workingDir.append( bin_dir( slash ) );
	dirSave.SetDllDir( workingDir );
	dirSave.SetWorkingDir( workingDir );

	if ( !util_load_deps() )
		return 1;

	QApplication app( argc, argv );

	std::unique_ptr< MainWindow > window( new MainWindow() );

	window->Initialize( &args );	
	window->StartRecord();
	window->show();

	int ret = app.exec();
	
	return ret;
}