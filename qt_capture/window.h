#pragma once

#include <QObject>
#include <QMainWindow>
#include <QWidget>
#include <QCloseEvent>

#include "def.h"
#include "util.h"

#include <libobs/obs.h>
#include <stdint.h>
#include <sstream>

class RenderDisplay : public QWidget 
{
public:
	inline RenderDisplay(QWidget *parent = 0, Qt::WindowFlags flags = 0)
		: QWidget(parent, flags)
	{
		setAttribute( Qt::WA_PaintOnScreen );
		setAttribute( Qt::WA_StaticContents );
		setAttribute( Qt::WA_NoSystemBackground );
		setAttribute( Qt::WA_OpaquePaintEvent );
		setAttribute( Qt::WA_DontCreateNativeAncestors );
		setAttribute( Qt::WA_NativeWindow );
	}

	virtual QPaintEngine *paintEngine( void ) const override { return nullptr; }
};

enum BaseEncodingType
{
	ENCODING_RAW = 0, // For AVI
	ENCODING_FLV,
	ENCODING_H264 // non FLV compatible h264
};

class MainWindow: public QMainWindow
{
	Q_OBJECT

private:
	RenderDisplay display;

	bool showWindow, isRawVideo, destroyed;

	obs_source_t* captureSource;
	
	obs_output_t* fileout;
	
	obs_encoder_t* videoEncoder;
	obs_encoder_t* audioEncoder;

	gs_vertbuffer_t* frame;

	struct capture_args* args;

	BaseEncodingType encoding;

	void DetermineEncoding( void );

	void SetupAudio( void );
	void SetupVideo( void );

	void SetupFlv( void );
	void SetupFFmpeg( void );

	void SetupSource( bool doScaling );

	void Destroy( void );

protected:
    void closeEvent( QCloseEvent* e );

public:
	 MainWindow( void );
	~MainWindow( void );

	obs_source_t* GetCaptureSource( void ) { return captureSource; }

	void Initialize( struct capture_args* args );

	void StartRecord( void );
	void StopRecord( void );

	static void Render( void* data, uint32_t cx, uint32_t cy );
};