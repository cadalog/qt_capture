#pragma once

#include <QSharedMemory>
#include <QThread>
#include <assert.h>
#include <string>

#include <Windows.h>
#include <direct.h>
#include "def.h"

struct capture_args
{
	int baseWidth = 0;
	int baseHeight = 0;

	int scaleWidth = 0; // optional; 0 if not specified
	int scaleHeight = 0; // optional; 0 if not specified

	int fps = 0;

	float closeDelay = 0.0f;    // optional; represents a time value, in seconds

	// all non-POD structs/classes should be placed after saveLocation 
	std::string saveLocation; 
	std::string exeName;
	std::string windowClass;
	std::string windowTitle;
};

class DirStack
{
public:
	char dllPath[ 4096 ];
	char workDir[ 4096 ];

	DirStack( void )
	{
		memset( dllPath, 0, sizeof( dllPath ) );	
		memset( workDir, 0, sizeof( workDir ) );

		GetDllDirectoryA( 4096, dllPath );
		GetCWD( workDir, 4096 );
	}

	void SetWorkingDir( const std::string& dir )
	{
		bool success = SetCurrentDirectoryA( dir.c_str() );
		assert( success );
	}

	void SetDllDir( const std::string& dir )
	{
		bool success = SetDllDirectoryA( dir.c_str() );
		assert( success );
	}

	~DirStack( void )
	{
		SetDllDirectoryA( dllPath );
		SetCurrentDirectoryA( workDir );
	}
};

static INLINE void util_alert( const std::string& msg )
{
	MessageBoxA( nullptr, msg.c_str(), "Alert", MB_OK | MB_ICONINFORMATION );
}

static INLINE std::string bin_dir( char dirSep )
{
	char sep[ 2 ] = { dirSep, '\0' }; 
	char str[ 32 ];
	memset( str, 0, sizeof( str ) );
	strcat( str, "bin" );
	strcat( str, sep );
	strcat( str, "64bit" );
	
	return std::string( str );
}

bool util_load_deps( void );