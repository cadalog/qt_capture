#include "window.h"

#include <libobs/util/platform.h>
#include <libobs/util/threading.h>
#include <libobs/graphics/matrix4.h>
#include <plugins/win-capture/graphics-hook-info.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <vector>
#include <memory>

#include <QMessageBox>
#include <QCoreApplication>

#define SOURCE_NAME "default_fileoutput"

#define VIDEO_BITRATE 2500
#define VIDEO_BUFSIZE VIDEO_BITRATE// 2500 // not used, since this is essentially the default for the actual OBS frontend; is kept here for reference in case it's needed
#define VIDEO_ENCODER_NAME "h264"

#define AUDIO_ENCODER_NAME "AudioTrack"
#define AUDIO_BITRATE 160
#define AUDIO_SAMPLES_PER_SEC 44100
#define AUDIO_BUFFERTIME_MS 1000

#define USE_CBR true

static os_sem_t* write_sem = nullptr;

static void nop()
{
}

static void my_log_handler( int log_level, const char *format, va_list args, void *param )
{
	char buf[ 4096 * 4 ] = {};
	vsprintf( buf, format, args );
	nop();
}

MainWindow::MainWindow( void )
	: display( this ),
	  showWindow( false ),
	  isRawVideo( false ),
	  destroyed( false )
{
	frame = nullptr;
	args = nullptr;
	fileout = nullptr;
	captureSource = nullptr;
	videoEncoder = nullptr;
	audioEncoder = nullptr;
}

MainWindow::~MainWindow( void )
{
	Destroy();
}

void MainWindow::Destroy( void )
{
	if ( !destroyed )
	{
		// Mild sanitization on the abs
		DWORD millisec = ( DWORD )abs( ( long )( args->closeDelay * 1000.0f ) );

		// 0 implies no delay. Also implies infinite sleep if passed as argument - DO NOT WANT
		if ( millisec != 0 )
		{
			Sleep( millisec );
		}

		StopRecord();

		obs_enter_graphics();
		gs_vertexbuffer_destroy(frame);
		obs_leave_graphics();

		obs_shutdown();

		destroyed = true;
	}
}

void MainWindow::closeEvent( QCloseEvent* e )
{
	QWidget::closeEvent( e );
    if ( e->isAccepted() )
    {
        Destroy();
    }
}

void MainWindow::DetermineEncoding( void )
{
	size_t start = args->saveLocation.find_last_of( '.' );

	const std::string& ext = args->saveLocation.substr( start, args->saveLocation.size() - start );

	if ( ext == ".avi" )
	{
		encoding = ENCODING_RAW;
	}
	else if ( ext == ".flv" )
	{
		encoding = ENCODING_FLV;
	}
	else
	{
		encoding = ENCODING_H264;
	}
}

void MainWindow::Initialize( struct capture_args* args0 )
{
	args = args0;

	bool doScaling = args->scaleWidth != 0 && args->scaleHeight != 0;

	int result = obs_startup( "en-US" );
	assert( result );
	base_set_log_handler( my_log_handler, nullptr );

	// Grab the extension; determine our file format
	DetermineEncoding();

	// Initialize audio/video settings; load framebuffer with which we can draw to
	{
		// video
		{
			struct obs_video_info ovi = {};

			int outputWidth, outputHeight, baseWidth, baseHeight;

			if ( doScaling )
			{
				baseWidth = args->baseWidth;
				baseHeight = args->baseHeight;
				
				outputWidth = args->scaleWidth;
				outputHeight = args->scaleHeight;
			}
			else
			{
				outputWidth = baseWidth = args->baseWidth;
				outputHeight = baseHeight = args->baseHeight;
			}

			ovi.fps_num = args->fps;
			ovi.fps_den = 1;
			
			ovi.graphics_module = "libobs-opengl.dll";
			
			ovi.base_width = baseWidth;
			ovi.base_height = baseHeight;
			
			ovi.output_width = outputWidth;
			ovi.output_height = outputHeight;

			ovi.window_width = outputWidth;
			ovi.window_height = outputHeight;

			ovi.output_format = VIDEO_FORMAT_NV12;
			ovi.colorspace = VIDEO_CS_709;
			ovi.range = VIDEO_RANGE_FULL;
			ovi.adapter = 0;
			ovi.gpu_conversion = false;
			ovi.scale_type = OBS_SCALE_BICUBIC;

#ifdef _WIN32
			ovi.window.hwnd = (HWND)display.winId();
#elif __APPLE__
			ovi.window.view = (id)display.winId();
#else
			ovi.window.id = display.winId();
			ovi.window.display = QX11Info::display();
#endif

			setMinimumWidth( outputWidth );
			setMinimumHeight( outputHeight );

			result = obs_reset_video( &ovi );
			assert( result == 0 );
		}

		display.setMinimumWidth( width() );
		display.setMinimumHeight( height() );

		display.show();

		// audio
		{
			struct audio_output_info ai;
			ai.name = "Main Audio Track";
			ai.format = AUDIO_FORMAT_FLOAT;

			ai.samples_per_sec = AUDIO_SAMPLES_PER_SEC; 
			ai.speakers = SPEAKERS_STEREO;
			ai.buffer_ms = AUDIO_BUFFERTIME_MS;

			result = obs_reset_audio( &ai );
			assert( result );
		}
		
		// add main render loop function, generate texture quad
		obs_add_draw_callback( Render, this );

		obs_enter_graphics();

		gs_render_start( true );
		
		gs_vertex2f( 0.0f, 0.0f );
		gs_vertex2f( 0.0f, 1.0f );
		gs_vertex2f( 1.0f, 1.0f );
		gs_vertex2f( 1.0f, 0.0f );
		gs_vertex2f( 0.0f, 0.0f );

		frame = gs_render_save();

		obs_leave_graphics();
	}

	obs_load_all_modules();

	// Initialize encoders and file output
	const std::string& outputName( encoding == ENCODING_FLV ? "flv_output" : "ffmpeg_output" );

	fileout = obs_output_create( outputName.c_str(), SOURCE_NAME, nullptr );
	assert( fileout );

	// We let OBS resolve most of the audio/video initialization by default if the user is dumping to .avi
	if ( encoding != ENCODING_RAW )
	{
		SetupVideo();
		SetupAudio();
	}

	if ( encoding == ENCODING_FLV )
	{
		SetupFlv();
	}
	else
	{
		SetupFFmpeg();
	}

	SetupSource( doScaling );
}

void MainWindow::SetupAudio( void )
{
	audioEncoder = obs_audio_encoder_create( "ffmpeg_aac", AUDIO_ENCODER_NAME, nullptr, 0 );
	assert( audioEncoder );

	obs_data_t* audioSettings = obs_data_create();

	obs_data_set_int( audioSettings, "bitrate", AUDIO_BITRATE );
	obs_encoder_set_name( audioEncoder, AUDIO_ENCODER_NAME );

	obs_encoder_update( audioEncoder, audioSettings );
	obs_data_release( audioSettings );

	obs_encoder_set_audio( audioEncoder, obs_get_audio() );
}

void MainWindow::SetupVideo( void )
{
	videoEncoder = obs_video_encoder_create( "obs_x264", VIDEO_ENCODER_NAME, nullptr );
	assert( videoEncoder );
	obs_encoder_set_video( videoEncoder, obs_get_video() );

	// FLV encoding requires a few settings, where as FFMpeg automates this
	if ( encoding == ENCODING_FLV )
	{
		obs_data_t* videoSettings = obs_data_create();
			
		obs_data_set_int( videoSettings, "bitrate", VIDEO_BITRATE );
		obs_data_set_int( videoSettings, "buffer_size", VIDEO_BUFSIZE );
		obs_data_set_bool( videoSettings, "cbr", true );
		obs_data_set_bool( videoSettings, "use_bufsize", false );
		
		obs_encoder_update( videoEncoder, videoSettings );

		obs_data_release( videoSettings );
	}

	// NOTE(holland): I don't think this is necessary, since the game capture source takes care of this already. However,
	// it is kept here just in case.
	//obs_encoder_set_scaled_size( videoEncoder, args->scaleWidth, args->scaleHeight );
}

void MainWindow::SetupFFmpeg( void )
{
	obs_data_t* settings = obs_data_create();
	
	obs_data_set_string( settings, "url", args->saveLocation.c_str() );

	obs_data_set_int( settings, "video_bitrate", VIDEO_BITRATE );
	obs_data_set_string( settings, "video_encoder", NULL );
	obs_data_set_string( settings, "video_settings", NULL );

	obs_data_set_int( settings, "audio_bitrate", AUDIO_BITRATE );
	obs_data_set_string( settings, "audio_encoder", NULL );
	obs_data_set_string( settings, "audio_settings", NULL );

	if ( encoding == ENCODING_RAW )
		obs_data_set_string( settings, "codec_type", "raw" );
	else 
		obs_data_set_string( settings, "codec_type", "" );

	obs_output_set_mixer( fileout, 0 ); // 0 => audio encoder index
	obs_output_set_media( fileout, obs_get_video(), obs_get_audio() );
	obs_output_update( fileout, settings );

	obs_data_release( settings );
}

void MainWindow::SetupFlv( void )
{
	obs_output_set_video_encoder( fileout, videoEncoder );
	obs_output_set_audio_encoder( fileout, audioEncoder, 0 );

	obs_data_t* settings = obs_data_create();
	obs_data_set_string( settings, "path", args->saveLocation.c_str() );
	obs_output_update( fileout, settings );
	obs_data_release( settings );
}

void MainWindow::SetupSource( bool doScaling )
{
	obs_data_t* settings = obs_data_create();

	obs_data_set_bool( settings, "capture_any_fullscreen", false );
	obs_data_set_int( settings, "priority", 2 ); // 2 refers to determining window to capture based primarily on EXE name (in this case, sketchup)
	obs_data_set_bool( settings, "capture_overlays", false );
	obs_data_set_bool( settings, "capture_cursor", false );
	obs_data_set_bool( settings, "sli_compatibility", false );
	obs_data_set_bool( settings, "force_scaling", doScaling );
	
	if ( doScaling )
	{
		std::stringstream resolution;
		resolution << args->scaleWidth << 'x' << args->scaleHeight;

		obs_data_set_string( settings, "scale_res", resolution.str().c_str() );
	}

	if ( args->windowTitle.empty() ) args->windowTitle.append( "dummy" );
	if ( args->windowClass.empty() ) args->windowClass.append( "dummy" );
	if ( args->exeName.empty() ) args->exeName.append( "dummy" );

	std::stringstream windowID;
	windowID << args->windowTitle << ":" << args->windowClass << ":" << args->exeName;

	// Format of third argument is <Window Title>:<win32 window class>:<exe name>; all three are required to
	// identify the target
	obs_data_set_string( settings, "window", windowID.str().c_str() );
	
	captureSource = obs_source_create( OBS_SOURCE_TYPE_INPUT, "game_capture", "Game Capture", settings );
	obs_add_source( captureSource );
	obs_set_output_source( 0, captureSource );

	obs_data_release( settings );
}

void MainWindow::StopRecord( void )
{
	obs_output_stop( fileout );
}

void MainWindow::StartRecord( void )
{
	bool startSuccess = obs_output_start( fileout );
	assert( startSuccess );
}

void MainWindow::Render( void* data, uint32_t cx, uint32_t cy )
{
	MainWindow* window = static_cast< MainWindow* >( data );

	bool renderToSource = true;

	gs_viewport_push();
	gs_set_viewport( 0, 0, window->width(), window->height() );

	gs_effect_t* solid = obs_get_solid_effect();
	gs_eparam_t* colorParam = gs_effect_get_param_by_name( solid, "color" );
	gs_technique_t* solidTech = gs_effect_get_technique( solid, "Solid" );

	gs_technique_begin( solidTech );
	gs_technique_begin_pass( solidTech, 0 );

	gs_projection_push();
	gs_matrix_push();
	gs_matrix_identity();
	{
		gs_ortho( 0.0f, ( float ) window->width(), 0.0f, ( float ) window->height(), -100.0f, 100.0f );

		vec4 background;
		vec4_set( &background, 0.0f, 0.0f, 0.0f, 1.0f );
		gs_effect_set_vec4( colorParam, &background );
		
		gs_matrix_scale3f( ( float ) cx, ( float ) cy, 1.0f );

		gs_load_vertexbuffer( window->frame );
		gs_draw( GS_TRISTRIP, 0, 5 );
	}
	gs_projection_pop();

	gs_matrix_pop();

	gs_technique_end_pass( solidTech );
	gs_technique_end( solidTech );

	gs_load_vertexbuffer( nullptr );

	obs_source_video_render( window->captureSource );

	gs_viewport_pop();
}