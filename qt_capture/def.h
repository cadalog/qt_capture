#pragma once

#ifndef _M_X64
#	error "Only x64 is supported"
#endif

#define INLINE inline
#define QT_PLATFORM_PLUGIN_PATH_KEY "-platformpluginpath"

#ifdef _WIN32
#	define GetCWD _getcwd
#	define SHARED_EXT ".dll"
#	define OBSRENDER_EXPORT __declspec(dllexport)
#	ifdef NDEBUG
#		define QT_PLATFORM_PLUGIN "qwindows"SHARED_EXT
#	else
#		define QT_PLATFORM_PLUGIN "qwindowsd"SHARED_EXT	
#	endif // NDEBUG
#else
#	error "Only Windows is supported"
#endif //_WIN32

#define CHAR_BUF_SIZE 1024
#define PATH_SIZE 4096